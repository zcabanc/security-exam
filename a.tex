\documentclass[a4paper,10pt]{article}
\usepackage{microtype}
\usepackage[margin=3cm]{geometry}
\usepackage[hidelinks]{hyperref}
\usepackage[hyperref]{xcolor}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{ulem}
\usepackage{longtable}
\setlength{\parindent}{0pt}
\setlength{\parskip}{1\baselineskip plus 4pt minus 8pt}
\lstset{
  basicstyle={\ttfamily\normalsize},
  numberstyle={\small\color[RGB]{100,100,100}},
  columns=fullflexible,
  showstringspaces=false,
  keepspaces=true,
  escapechar=@,
  commentstyle=\color[RGB]{127,0,127},
  keywordstyle=\color[RGB]{0,127,0},
  stringstyle=\color[RGB]{127,0,0},
  tabsize=2,
  framesep=0pt,
  aboveskip=0.5\baselineskip,
  belowskip=0.5\baselineskip,
}

\begin{document}
\centerline{Candidate number: \texttt{KWYM1}}

\section*{Question 1}

\paragraph{1.1}
Cleartext: \texttt{congratulationsonfindingtheplaintext}

Code (Wolfram Language): \\
\includegraphics[trim={44pt 344pt 140pt 74pt}, width=1\linewidth, clip]{1.pdf}

\paragraph{1.2}
$a = 10, b = 103$ gives a secret $g^{ab} \mod p = 1$.

Code (Wolfram Language): \\
\includegraphics[trim={47pt 644pt 53pt 72pt}, width=1\linewidth, clip]{2.pdf} \\
\includegraphics[trim={47pt 458pt 53pt 152pt}, width=1\linewidth, clip]{2.pdf}

\paragraph{1.3} True, as long as we can rule out trusted CAs mis-issuing certificates.

\paragraph{1.4} Since a cryptographic hash function behaves as a pseudorandom number generator which outputs values that uniformly distribute in the output space, $\displaystyle P(H(2) = 0) = \frac{1}{2^{60}}$.

\paragraph{1.5} As long as the size of the input space is larger than the size of the output space, the probability of at least one collision is 1 because of the pigeonhole principle.

\paragraph{1.6} This is equivalent to CTR mode but with a pre-determined nonce. However, since nonce must never be re-used for different messages, determining nonce based on current time is not secure, because:

\begin{itemize}
  \item Since this is a symmetric cipher, both Alice and Bob needs to ensure they will never send two messages at the exact same millisecond, which is unlikely but still possible.
  \item Even if they can ensure the above, they still has the risk of re-using nonce if they encrypted messages before and after a backward clock adjustment caused by e.g.\ leap seconds, or even just BST/GMT changes if it is implemented poorly.
\end{itemize}

\section*{Question 2}

\paragraph{2.1}

\begin{enumerate}
  \item \textbf{Threat}: intelligence agents (IA from here on) may take down any control server of Bob's botnet. \\
        \textbf{Vulnerability}: one or more servers' controlling those bots may have their IP or domain blocked nationwise. \\
        \textbf{Impact}: Botnet is shut down, possibly resulting in Bob needing to restart from scratch, if the IP or domain is hard-coded. Bob may also be banned from his service providers. \\
        \textbf{Protection cost}: Requires some careful engineering.
  \item \textbf{Threat}: IA or other competing hackers may exploit infected machines for their own purpose. \\
        \textbf{Vulnerability}: Bob's botnet software may execute arbitrary commands without proper verification that it's from Bob. \\
        \textbf{Impact}: Bob losing control of his botnet, possibly resulting in botnet shut down (if self-destruct command injected) or botnet being used in a way that increases Bob's legal liability, should he be caught. \\
        \textbf{Protection cost}: Requires some careful engineering.
  \item \textbf{Threat}: IA may find out about Bob's identity \\
        \textbf{Vulnerability}: Bob's service provider may be forced to reveal any information they have about Bob. \\
        \textbf{Impact}: Legal liability or criminal conviction. \\
        \textbf{Protection cost}: May need to use non-mainstream, offshore ISPs that can be less reliable and/or more expensive.
\end{enumerate}

\paragraph{2.2}

\begin{enumerate}
  \item Make each bot use Tor to communicate with control servers, as well as using bridges that obfuscate the connection to look like normal HTTP.
  \begin{itemize}
    \item[$+$:] Can be made very hard to detect.
    \item[$+$:] Also helps with vulnerability 3.
    \item[$-$:] Slower than direct connection and may not be reliable all the time.
    \item[$-$:] Requires botnet software to bundle Tor, which causes it to be larger and may be an identifiable trait by antivirus.
  \end{itemize}
  \item Use cryptography signatures to sign and verify commands, or use a secure communication channel between bots and servers.
  \begin{itemize}
    \item[$+$:] Protocols that are carefully designed can almost completely (except programming mistakes) prevent such misuse.
    \item[$-$:] Requires more effort to write, and results in larger bot software needed, which can impact the probability of successfully infecting machines.
  \end{itemize}
  \item Use offshore service providers and pay them with cryptocurrencies.
  \begin{itemize}
    \item[$+$:] Highly anonymous
    \item[$-$:] Can be more expensive and less reliable then mainstream ISPs.
  \end{itemize}
\end{enumerate}

There's nothing preventing Bob from doing all three things together, and it gives better protection.

\paragraph{2.3}

Public key cryptography. TLS may be used for the connection (above other protocols such as Tor) with an embedded CA signed by Bob (and reject all others). This avoids the need to develope another protocol and is therefore more cost-effective. Bob can then issue certificates for all the control servers that the bot will connect to, so this approach will support any number of servers. However, this does not allow a peer-to-peer networks where hosts that can't connect to the server receives commands relayed by other hosts. Additional signature protocol may be needed to support that.

\vfill
\centerline{See next page for the rest}
\vfill

\clearpage
\section*{Question 3}

Firefox 88.0 on Linux:

\newcommand{\greylockicon}{\raisebox{-3pt}{\includegraphics[width=1em]{lock-with-warn.pdf}}}
\newcommand{\lockicon}{\raisebox{-3pt}{\includegraphics[width=0.75em]{lock.pdf}}}
\newcommand{\infoicon}{\raisebox{-2pt}{\includegraphics[width=1em]{info.pdf}}}

{
\setlength{\LTleft}{-20pt}
\begin{longtable}{r|p{55pt}|p{0.24\linewidth}|p{0.24\linewidth}|p{0.24\linewidth}}
  \hline
  \textbf{Page} & \textbf{Icon} & \textbf{Warnings} & \textbf{Steps} & \textbf{Vulnerability} \\
  \hline
  \texttt{self-signed} & \greylockicon, negative &
    Instead of page content, it shows ``Warning: Potential Security Risk Ahead''. This is a negative indicator. &
    Full page warning already shows something is wrong. &
    The website is using a certificate that is signed by itself, rather than a trusted CA.
    If user ``Accept the Risk and Continue'', their communication with the website might be eavesdropped since there is usually no way for the user to know whether the certificate is truly owned by the website, or created by a Man-in-the-Middle (MitM from here on). \\
  \hline
  \texttt{dh1024} & \infoicon, security wise neutral (this icon is also used for all other error pages) &
    Can't connect, with error \verb|SSL_|\allowbreak\verb|ERROR_|\allowbreak\verb|NO_|\allowbreak\verb|CYPHER_OVERLAP|. Security wise this is neutral. &
    Connection error already shows something is wrong. &
    The website is using a TLS cipher that is too insecure. In this case there is nothing the user could do to continue, so no attacks exploting this weak cipher can happen. \\
  \hline
  \texttt{no-sct} & \lockicon, positive &
    No warnings at all (positive). &
    There is nothing obvious that can tell me that something is wrong. I clicked on the lock icon, expanded the detail, clicked ``More information'', ``View certificate'' but (as expected) did not see any `` Embedded SCTs'' extension. &
    The website is using a certificate that does not have any embedded SCTs. This means that unless my browser managed to get an SCT for the certificate via other means, the browser can not be sure that the true owner of this website is aware that this certificate has been issued for them. Since no warning is shown to the user, a malicious or compromised CA may issue a certificate of \texttt{badssl.com} and use it to do MitM attacks, without users or the website owner being aware of this.\\
  \hline
  \texttt{mixed-script} & \lockicon, positive &
    No warnings in the main page (positive). &
    I clicked on the lock icon, and this message is shown below {\color{green!50!black}Connection secure}:
    ``\textit{Firefox has blocked parts of this page that are not secure.}'' &
    The website is trying to load a script from a \texttt{http} URL. This is insecure because an attacker can MitM the connection and modify the script loaded to send or alter any content in the secure page. However the browser has prevented this from happening, so attacks would not be successful on its own, unless the user expands the detail and clicks on ``Disable protection for now''. \\
    \hline
\end{longtable}
}

Chromium 90.0.4430.93 on Linux:

{
\setlength{\LTleft}{-20pt}
\begin{longtable}{r|p{55pt}|p{0.24\linewidth}|p{0.24\linewidth}|p{0.24\linewidth}}
  \hline
  \textbf{Page} & \textbf{Icon} & \textbf{Warnings} & \textbf{Steps} & \textbf{Vulnerability} \\
  \hline
  \texttt{mixed-script} & \lockicon, positive &
    No warnings at all (positive). &
    Nothing suggests that something has gone wrong. (Only if the user press \texttt{F12} would they be able to see a mixed content warning.) &
    In Chromium it is not possible for the user to ``disable protection'', hence there is no way for such an attack to happen. \\
  \hline
\end{longtable}
}

\section*{Question 4}

\paragraph{4.1}

\begin{enumerate}
  \item No. SQL injection does not depend on the ability to send spoofed packages or being able to send from a large number of IPs. The attackers can carry out their attacks with their own IP (or behind a VPN they control).
  \item The possibility of MitM is a consequence of the network not being authenticated. If there is a mechanism to ensure source IP in a package is always the true IP of the sender, and the sender is defined as the peer which constructed the package, the attacker would not be able to send anything on behalf of the victim. Hence \textbf{Yes}. This does not, however, mean that passive eavesdropping is not possible.
  \item This would help to some extent, since the server would be able to better block any attacker, and unless they can control a large amount of IPs, they would not be able to keep attacking for very long.
  \item No. The actual connections which causes a CSRF are made by the user's browser, not spoofed by the attacker.
  \item No. The actual connections which relates to an XSS attack (such as submitting confidential information to attacker controlled servers) are made by the user's browser, not spoofed by the attacker.
\end{enumerate}

\paragraph{4.2}

Timing information. By comparing the time it takes for the check to happen, the attacker can learn, roughly, what prefix of the password they submitted contains only 1 wrong characters, and hence iteratively guess the correct password a lot faster than brute-force.

To fix it while perversing this ``feature'', count up how many characters are wrong, and only return whether this number is $> 2$ after going through all of the input. Terminate early if no more input to be read (but the correct password still has more characters), and don't terminate early if the correct password has ended but input still has more characters. This prevents leaking any information about the length of the password as well.

\paragraph{4.3} Assuming that the page inserts the content of \texttt{q} directly into the HTML:

\verb|https://university.ac.uk/search?q=%3Cscript%3Ealert(1)%3C%2Fscript%3E| \\
(\texttt{q} = \verb|<script>alert(1)</script>|)

Impacts:

\begin{enumerate}
  \item An attacker looking to phish logins may be able to craft an URL that seems to be from the university website (with the correct domain) that when visited will actually show a login form that sends information to the attacker's server. This can be used to send more convincing phishing emails.

  \item An attacker may be able to craft an URL on the university website that collects private information, if the university's student portal is also hosted on the website and has persistent login. The XSS payload can redirect the user into another announcement page to make it seem like nothing bad has happened. The attacker can then proceed to spread those ``announcement'' links around on social media or student group chats, and the link may even be forwarded by unsuspecting students, causing a wider impact.
\end{enumerate}

\end{document}
